Output of reproducible paper template
=====================================

This repository is made to keep the output of the [reproducible paper
template](https://gitlab.com/makhlaghi/reproducible-paper). Please see
that repository for a full description of reproducible papers and the
implementation. The outputs of that repository are kept in this
"convenience" repository for fast and easy availability of the outputs
without bloating the main repository.